# Imperator Modding
## Modding Tool For Imperator Rome

### Table of contents
1. What is this tool?
2. How do I report a bug?
3. How can I get involved?
  1. Ideas/suggestions
  2. Translations
  3. Mod support
  4. UI designs/generic artwork
  5. Code
    1. Languages
    2. Coding standards
  


## 1. What is this tool?
This tool shall contain everything needed for someone with little to no knowledge of modding to be able to create a mod.

This tool will contain the basics for aspects of the game it touches, but may not be able to cater for more advanced mods. For example, when creating a mod that deals with trade, this tool will be able to deal with the base game resources, but may not have the capacity for custom resources.

This tool may be adapated to include support for popular mods, though this is at the discretion of those maintaining the tool, and not all mods will be supported.

## 2. How do I report a bug?
Whilst you can submit a bug report anywhere this tool is advertised, it is best to submit bug reports on the offical site, as that way the people who can fix the bug(s) will be able to see your issue straight away.

## 3. How to I get involved?
Whether you want to suggest something, your an artist willing to design things for the tool, you speak a second language, or you are a web developer, any and all involvement is very much appreciated.

See the below to see how you can get involved in the project.

### 1. Ideas/suggestions
Not all ideas and suggestions will be taken on board (in the form given), however, all are appreciated. 

You can make suggestions for what you'd like to see added/changed on the official website.

### 2. Translations
If you speak a language other than English, then it would be great if you could help out with translating parts of the site. Whether you can just afford enough time to translate menus, or you could translate the entire site, being able to enable a larger proportion of the community is the ideal goal.

[Info on how to become a translator to come.]

### 3. UI designs/generic artwork
If you think part of the tool could do with some work, or you would like to see your art used in many mods, then your skillset would be greatly appreciated.

[Info on how to become an artist to come.]

### 4. Code
Are you a web developer? Whether you've got a few months or a few years experience, if you feel you could contribute to the project, your assistance is greatly appreciated.

#### 1. Languages
The languages used in this project are:
* HTML
* SASS - compiled to CSS
* Javascript - using the jQuery library
* PHP - using the Laravel framework
* MYSQL - managed by Laravel

#### 2. Coding standards

##### Indentation
* Indentation is to be tabs only.
* Tab size should be set to 4.

##### Braces
Opening braces should never sit on their own line, whilst closing braces should always have their own line, unless being proceeded by an else statement.

A space between a closing bracket and brace ") {" is optional.

###### Example
```
if( true ) {
    //Is true
} else {
    //Is false
}
```

##### Function Naming
All function names should describe what it is doing, with an "is true" descriptor.

###### Example
We may want to check that a user is deactived before running a particular bit of code.

```
function isUserActive( user ) {
    return (user == active);
}
```

##### Commenting
All functions must have a description of what the function does, in natural language, before being declared. With a short line of = to separate description and parameters.

If the function has any parameters, this description should contain what that parameter should contain

###### Example
```
/**
* Return whether queried user is currently active
* ===========
* user - JSON object of user
*/
function isUserActive( user ) {
```

Comments within blocks of code should be short. If you need massive amounts of text to explain the code, then it's probably too complex, or should be delegated to another function

These comments should always be // comments, apart from in CSS which should always use /* \*/ comments.

###### Example
```
//Check if the user is active
return (user == active)
```

Only use comments in blocks of code if you feel an explanation may be needed for why something is done this way; e.g. doing a check on !true instead of false, or if you feel other developers, or your future self, may come to this code and question what it's actually doing something for

##### File naming
CSS, Javascript, and view files should always be lowercase only, and hypen separated.

###### Example
```
script.js
style.css
mobile-menu.scss
index-page.php
```

Class-based files should be named in UpperCamelCase.

###### Example
```
Users.php
UserRoles.php
```

##### Function Naming
All functions should be in standard camelCase.

###### Example
```
function isUserActive...
function getNewUsers...
```

##### Variable Naming
All variables should be in standard camelCase, unless they are from the database, in which they should be snake_case.

Variables should always describe what the variable is.

###### Example
```
Non-MySQL:
var firstName;
$lastName;

MySQL:
first_name
last_name
```

##### Views
Logic should not be found in any view file which is not a partial. Partials are sub-views which are to be used across the site, and have specific roles.

If you create a partial which has logic, put a comment at the top of the file on how to include the view, with what parameters need to be passed, at the top.


##### Browser Support
The following browsers are all that we support. If something works on older browsers, great. But if not, then anything which require major work will not be accomodated.

* Google Chrome : Up to 2 versions prior
* Firefox : Up to 2 versions prior
* Safari : Current version
* Edge : Current version
* Internet Explorer 11

Due to Internet Explorer 11 not supporting some quality of life CSS properties, this is to be supported at a degraded level; Meaning as long as the page looks and functions as intended on IE, certain extra features can be overlooked. For example, the use of `position: sticky`, although not supported on IE, this is preferred over a `position: fixed` + Javascript solution.

When writing CSS, you do not need to worry about prefixes, such as -webkit, the compiler will do this for you.

###### Compiling files
All files you edit will need compiling. This is done via gulp tasks.

The following tasks are available

* `gulp` - This will run all gulp tasks once
* `gulp watch` - This will run all gulp tasks whenever you save a file
* `gulp styles` - This will compile the CSS files once
* `gulp scripts` - This will compile the Javascript files once

##### Inline styles
The only time inline styling is permitted is when used for dynamic elements, such as background images on banners, or user-defined styles.

##### Writing CSS
All CSS is to be written in SCSS, and compiled to CSS via a compiler - At the time of writing, gulp.

##### Class Naming
Classes should always be fully lowercased, seperated by hypens instead of spaces, unless the class is used as a Javascript targetting class, or you are specifying a modification to an existing class.

Javascript targetting classes should be camelCase, whilst modifying class names should be connected via an underscore.

###### Example
```
<div class="banner-wrapper banner-size_large createSlider">
```

##### Element Targeting
When targetting an element via Javascript, create a specific class/ID to target the element(s) you wish to modify.

When targetting an element to style, via CSS, only target based on classes. Do not target via IDs or tags, unless you require different styling based on different attribute states.

###### Example
```
.tab{
    display: none;
    &:[data-active="true"]{
        display: block;
    }
}
```

##### Forbidden properties
When writing CSS, the following properties are forbidden unless specific conditions are met.

* `!important` : Cannot be used unless you are trying to style a widget which is inserted via external code.
* `float` : Float are not be used anywhere. If you require float, you're doing something wrong.

##### File Structure
Try to keep each section of the website confined to its own file, unless there is crossover with nesting elements.

For example, everything regarding to the search form, should be in a file `_search-form.scss` whilst the styling for the results may be in `_search-results.scss`

##### Variables and Mixins
Any time you are using a setting a CSS property which is the same somewhere else, e.g. text colours, use variables which are set in the `settings.scss` file. If the colour, pixel measurement, etc. is not in that file, then create a new variable for it. If we decide to change a colour used across the site in 6 months time, we don't want to spend 6 hours replacing it in every file.

When creating grid systems on the site, use the predefined grid mixins; flex-grid should be used when the content is repeatable, or you do not know how many children an element will have, e.g. search results, due to IE not supporting repeatable grid rows.

##### Media Queries/Responsive Layouts
When using media queries, use the breakpoint variables

###### Example
```
@media screen and (min-width: $tablet){
	.heading{
		font-size: $font-large;
	}
}
```

##### Controllers and contracts
Controllers should be used solely for dealing with a route. The only code found in a controller should be that which passes any data over to the contract, which is passed via the route. **default Laravel routes/controller methods do not need to altered**

Contracts work exactly like a controller would, but allow for the code to be seperated out into more specialised folder. For example, the admin controller may have 50 functions in it, but they send the data to be processed amongst 10 different contracts.

We do this to keep the controllers readable, and becomes more apparent for its uses when you have multiple methods in a controller which do complex logic, before returning a result.


##### Migrations
Migrations should be named in lower snake case

###### Example
```
create_roles_table
```